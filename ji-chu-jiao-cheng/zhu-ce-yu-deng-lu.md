# 注册与登录

> 事实上这个教程您可以完全不需要看，我们更希望您能够自主解决这种问题。

{% hint style="info" %}
若您未登录账号，您在进入 轻云 时见到的应为此界面。
{% endhint %}

<figure><img src="../.gitbook/assets/Snipaste_2023-02-11_22-28-51.png" alt=""><figcaption><p>轻云 登录界面</p></figcaption></figure>

## 注册

{% hint style="info" %}
若您是第一次使用 轻云，请先注册一个账号。
{% endhint %}

首先，在登录页面点击【现在注册】

<figure><img src="../.gitbook/assets/Snipaste_2023-02-11_22-30-45.png" alt=""><figcaption><p>点击【现在注册】</p></figcaption></figure>

然后，您将看到注册页面，按照要求填写相关信息后，再点击【我已阅读并同意 [服务条款](https://cloud.qyunt.com/newsview?id=1) 和 [隐私政策](https://cloud.qyunt.com/newsview?id=1)】前面的 □ 把它勾上箭头，然后点击【注册】。

{% hint style="info" %}
由于特殊原因，轻云目前强制手机号注册，在一段时间内可能不会作出改变。
{% endhint %}

<figure><img src="../.gitbook/assets/Snipaste_2023-02-11_22-39-04.png" alt=""><figcaption><p>点击【注册】</p></figcaption></figure>

## 登录

{% hint style="info" %}
若您已有 轻云 账号，可进行登录操作。
{% endhint %}

打开登录页面，填写相关信息后，点击【登录】即可。

<figure><img src="../.gitbook/assets/Snipaste_2023-02-11_22-47-17.png" alt=""><figcaption><p>点击【登录】</p></figcaption></figure>
